import React from 'react';
import PropTypes from 'prop-types';

import './Pagination.scss';

/**
 * Pagination
 * @description [Description]
 * @example
  <div id="Pagination"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Pagination, {
    	title : 'Example Pagination'
    }), document.getElementById("Pagination"));
  </script>
 */
class Pagination extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'pagination';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				Pagination
			</div>
		);
	}
}

Pagination.defaultProps = {
	children: null
};

Pagination.propTypes = {
	children: PropTypes.node
};

export default Pagination;
